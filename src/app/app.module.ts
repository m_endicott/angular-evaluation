import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule, routingComponents } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UserAddComponent } from "./components/user-add/user-add.component";
import { UserCardComponent } from "./components/user-card/user-card.component";

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    UserAddComponent,
    UserCardComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
