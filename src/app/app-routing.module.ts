import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { UserInfoComponent } from "../app/components/user-info/user-info.component";
import { UsersComponent } from "../app/components/users/users.component";

const routes: Routes = [
  { path: "", component: UsersComponent },
  { path: "user/:id", component: UserInfoComponent },
  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
export const routingComponents = [UserInfoComponent, UsersComponent];
