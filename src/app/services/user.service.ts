import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";

// Models
import { Card } from "../models/Card";
import { User } from "../models/User";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json"
  })
};

@Injectable({
  providedIn: "root"
})
export class UserService {
  baseUrl: string = "http://localhost:3000";

  constructor(private http: HttpClient) {}

  // Get All Users
  getUsers(): Observable<User[]> {
    return this.http.post<User[]>(`${this.baseUrl}/person/list`, httpOptions);
  }

  // Get Specified User
  getSingleUser(id: number): Observable<User[]> {
    return this.http.post<User[]>(
      `${this.baseUrl}/person/get`,
      { id },
      httpOptions
    );
  }

  // Get User Info
  getUserInfo(id: number): Observable<any> {
    return this.http.post(`${this.baseUrl}/person/get`, { id }, httpOptions);
  }

  // Get User Cards
  getUserCards(id: number): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/card/query`,
      { person: id },
      httpOptions
    );
  }

  // Add new card
  addCard(person_id: number, card_number: number): Observable<any> {
    return this.http.post(
      `${this.baseUrl}/card/add`,
      { person_id, card_number },
      httpOptions
    );
  }

  // Toggle Completed
}
