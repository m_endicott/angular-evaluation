import { Component, OnInit } from "@angular/core";
import { UserService } from "../../services/user.service";

import { User } from "../../models/User";
import { Card } from "../../models/Card";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"]
})
export class UsersComponent implements OnInit {
  users: User[];
  id: number;

  constructor(private userService: UserService) {}

  ngOnInit() {}

  onSearch(id: number) {
    if (!id) {
      this.userService.getUsers().subscribe(users => {
        this.users = users;
      });
    }
    this.userService.getSingleUser(+this.id).subscribe(user => {
      if (user) {
        let searchArr = [];
        searchArr.push(user);
        this.users = searchArr;
      }
    });
  }
}
