import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-user-add",
  templateUrl: "./user-add.component.html",
  styleUrls: ["./user-add.component.scss"]
})
export class UserAddComponent implements OnInit {
  @Output() addCard: EventEmitter<any> = new EventEmitter();

  card_number: number;

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    const card = {
      card_number: this.card_number
    };

    this.addCard.emit(card);
  }
}
