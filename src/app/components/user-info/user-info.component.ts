import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

import { User } from "../../models/User";
import { Card } from "../../models/Card";

import { UserService } from "../../services/user.service";

@Component({
  selector: "app-user-info",
  templateUrl: "./user-info.component.html",
  styleUrls: ["./user-info.component.scss"]
})
export class UserInfoComponent implements OnInit {
  id: number;
  user: User;
  cards: Card[];

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {

  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = +params.get("id");
    });
    this.userService.getUserInfo(this.id).subscribe(user => {
      this.user = user;
      this.userService.getUserCards(this.id).subscribe(cards => {
        this.cards = cards;
      });
    });
  }

  addCard(card: Card) {
    this.userService.addCard(this.user.id, +card.card_number).subscribe(newCard => {
      this.cards.push(newCard);
    });
  }
}
